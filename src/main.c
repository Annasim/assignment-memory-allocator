#include "mem.h"
#include <string.h>

int* intArr;
int* bigIntArr;
int* bigIntArr2;
char* str;
char* str1;
char* str2;
int64_t* BIG_ARR;

void test_1()
{
	size_t intCount = 20;
	intArr = (int*)_malloc(sizeof(int) * intCount);
	printf("%p\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		intArr[i]=i+1;
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
}

void test_2()
{
	size_t intCount = 20;
	_free(intArr);
	intArr = (int*)_malloc(sizeof(int) * intCount);
	printf("%p\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
}

void test_3()
{
	str = (char*)_malloc(7);
	printf("%p\n", (void*)str);
	str[0] = 's'; str[1] = 't'; str[2] = 'r'; str[3] = 'o'; str[4] = 'k'; str[5] ='a'; str[6] = '\0';
	printf("%s", str);
}

void test_4()
{
	size_t intCount = 20;
	printf("%p\n", (void*)intArr);
	for(size_t i = 0; i < intCount; i++)
		printf("%d\n", intArr[i]);
}

void test_5()
{
	char str_tmp[30] = "ab";
	_free(intArr);
	str1 = (char*)_malloc(strlen(str_tmp)+1);
	str2 = (char*)_malloc(strlen(str_tmp)+1);
	for(size_t i = 0; i < strlen(str_tmp); i++)
	{
		str1[i] = str_tmp[i];
		str2[i] = str_tmp[strlen(str_tmp) - i - 1];
	}
	
	str1[strlen(str_tmp)] = '\0';
	str2[strlen(str_tmp)] = '\0';
	
	printf("%p\n", (void*)str1);
	printf("%s", str1);
	printf("%p\n", (void*)str2);
	printf("%s", str2);
}

void test_6()
{
	size_t intCount = 100;
	bigIntArr = (int*)_malloc(sizeof(int)*intCount);
	printf("%p\n", (void*)bigIntArr);
	
	bigIntArr2 = (int*)_malloc(sizeof(int)*intCount);
	printf("%p\n", (void*)bigIntArr2);
}

int main(int arg, char** args)
{
	test_1();
	test_2();
	test_3();
	test_4();
	test_5();
	test_6();
	return 0;
}
